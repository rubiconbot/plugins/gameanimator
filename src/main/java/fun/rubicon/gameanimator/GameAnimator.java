/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package fun.rubicon.gameanimator;

import fun.rubicon.plugin.Plugin;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class GameAnimator extends Plugin {

    private Timer timer;
    private Game[] gameArray;

    @Override
    public void init() {
        this.timer = new Timer();
    }

    @Override
    public void onEnable() {
        gameArray = new Game[]{
                Game.listening("rc!help"),
                Game.playing(String.format("on Version %s", getRubicon().getVersion())),
                Game.watching(String.format("at %s servers", getRubicon().getShardManager().getGuilds().size())),
                Game.watching(String.format("at %s users", getRubicon().getShardManager().getUsers().size()))
        };

        getRubicon().getShardManager().setStatus(OnlineStatus.ONLINE);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ThreadLocalRandom random = ThreadLocalRandom.current();
                getRubicon().getShardManager().setGame(gameArray[random.nextInt(0, gameArray.length - 1)]);
            }
        }, 0, 30000);
    }

    @Override
    public void onDisable() {
        timer.cancel();
    }

}
